import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

// Test Profile
const profile = {
  style: {
    width: 50,
    height: 50,
  },
  source: {
    uri: 'https://secure.gravatar.com/avatar/fafd9350cc34db0cf89c11a2adc58ec1?s=180&d=identicon',
  },
}

export class Profile extends Component {
  render() {
    return (
      <View>
        <Image
          style={profile.style}
          source={profile.source}></Image>
        <Text>Profile Test {this.props.profile.gender}!</Text>
      </View>
    )
  }
}