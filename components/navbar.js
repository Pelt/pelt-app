import React, { Component } from 'React';
import { StyleSheet, Text } from 'react-native';
import NavigationBar from 'react-native-navbar';
import Icon from 'react-native-vector-icons/FontAwesome';


const styles = {
  container: {
    flex: 1,
    backgroundColor: '#F00',
  },
};

const homeIcon = (<Icon name="home" size={30} color="#900" />);

const rightButtonConfig = {
  title: 'Menu',
  icon: homeIcon,
  handler: () => alert('hello!'),
};

const titleConfig = {
  title: 'Hello, world',
};

export class Navbar extends Component {
  render() {
    return (
      <NavigationBar
        title={titleConfig}
        rightButton={rightButtonConfig}
      />
    );
  }
}