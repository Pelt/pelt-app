import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Profile } from './profile';

export class ProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      type: 'LOADING',
      data: null,
    };
  }

  componentDidMount() {
    fetch('https://randomuser.me/api/')
    .then((response) => {
      if (response.status >= 400) {
        this.setState({
          type: 'ERROR',
          data: null,
        })
      }
      return response.json();
    })
    .then((output) => {
      this.setState({
        type: 'LOADED',
        data: output
      });
    });
    // setTimeout(() => this.setState({
    //   type: 'LOADED',
    //   data: [1, 2, 3]
    // }), 3000)
  }

  render() {
    const renderProfile = () => {
      if(!!this.state.data) {
        // return this.state.data.results.map((i,pos) => <Text key={pos}>{i.gender}</Text>);
        return <Profile profile={this.state.data.results[0]}></Profile>
      }
    }

    return <View>
      <Text>ProfileScreen Page!</Text>
      <Text>{this.state.type}</Text>
      {/*{this.state.data.map(i => <Text key={i}>{i}</Text>)}*/}
      {/*<Text>Test 1 - {Object.keys((this.state.data||{}).results).toString()}</Text>*/}
      {renderProfile()}
    {/*<Text>{this.state.data.info.version}</Text>*/}
    </View>;
  }
}
