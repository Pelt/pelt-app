import React, { Component } from 'react';
import { Text } from 'react-native';

export class Greeting extends Component {
  render() {
    return (
      <Text>G'day {this.props.name}!</Text>
    );
  }
}
