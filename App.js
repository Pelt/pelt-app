import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ContentView, Image, TouchableOpacity } from 'react-native';
import { Navbar } from './components/navbar.js';
// import { Greeting } from './components/greeting.js';
import SideMenu from 'react-native-side-menu';
import Menu from './components/menu';
import { HomeScreen } from './components/screenHome';
import { ProfileScreen } from './components/profilescreen';
// import 'isomorphic-fetch'

const menuItemSelect = (item) => {
  alert('Clicked!');
  console.log('item',item);
}

const styles = StyleSheet.create({
  button: {
    position: 'absolute',
    top: 20,
    padding: 10,
  },
  caption: {
    fontSize: 20,
    fontWeight: 'bold',
    alignItems: 'center',
  },
  container: {
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const screens = {
  Home: HomeScreen,
  Profile: ProfileScreen,
}

// const image = require('./assets/menu.png');

export default class App extends React.Component {
  static navigationOptions = {
    title: 'Welcome',
  };

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: 'Home',
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  onMenuItemSelected = item => {
    this.setState({
      isOpen: false,
      selectedItem: item,
    });
  }

  render() {
    const menu = <Menu onItemSelected={this.onMenuItemSelected} />;
    const screen = React.createElement(screens[this.state.selectedItem], {})
    // const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <SideMenu
          menu={menu}
          isOpen={this.state.isOpen}
          onChange={isOpen => this.updateMenuState(isOpen)}
          
        >
          <Navbar/>
          <View style={styles.container}>
            {screen}

            {/*<Text style={styles.welcome}>
              Welcome to React Native!
            </Text>
            <Text style={styles.instructions}>
              To get started, edit index.ios.js
            </Text>
            <Text style={styles.instructions}>
              Press Cmd+R to reload,{'\n'}
              Cmd+Control+Z for dev menu
            </Text>
            <Text style={styles.instructions}>
              Current selected menu item is: {this.state.selectedItem}
            </Text>*/}
          </View>
          <TouchableOpacity
            onPress={this.toggle}
            style={styles.button}
          >
            {/*<Image
              source={image}
              style={{ width: 32, height: 32 }}
            />*/}
          </TouchableOpacity>
        </SideMenu>
        {/*<Menu onItemSelected={menuItemSelect}/>*/}
        {/*<Text>Open up App.js to start working on your app!</Text>
        <Text>Changes you make will automatically reload.</Text>
        <Text>Shake your phone to open the developer menu.</Text>
        <Greeting name='Zeus'></Greeting>
        <Text>BLURG 2</Text>*/}
      </View>
    );
  }
}

// const styles = StyleSheet.create({
//   container: {
//     // flex: 1,
//     // backgroundColor: '#fff',
//     // alignItems: 'center',
//     // justifyContent: 'center',
//     paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
//     // backgroundColor: '#000',
//   },
// });
